<?php

use App\Entity\AuthToken;
use App\Entity\Task;
use App\Entity\User;
use App\Repository\AuthTokenRepository;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Infrastructure\DBAL\EntityRepository;
use Infrastructure\DBAL\ValueObject\Config;
use Infrastructure\DBAL\ValueObject\Connection;
use Infrastructure\DependencyInjection\Reference\ParameterReference;
use Infrastructure\DependencyInjection\Reference\ServiceReference;

return [
    Config::class => [
        'class' => Config::class,
        'arguments' => [new ParameterReference('dbal')]
    ],
    Connection::class => [
        'class' => Connection::class,
        'arguments' => [new ServiceReference(Config::class)]
    ],
    EntityRepository::class => [
        'class' => EntityRepository::class,
        'arguments' => [new ServiceReference(Connection::class)]
    ],
    UserRepository::class => [
        'class' => UserRepository::class,
        'arguments' => [
            new ServiceReference(Connection::class),
            new User(),
            'users'
        ]
    ],
    AuthTokenRepository::class => [
        'class' => AuthTokenRepository::class,
        'arguments' => [
            new ServiceReference(Connection::class),
            new AuthToken(),
            'auth_token'
        ]
    ],
    TaskRepository::class => [
        'class' => TaskRepository::class,
        'arguments' => [
            new ServiceReference(Connection::class),
            new Task(),
            'tasks'
        ]
    ],
];