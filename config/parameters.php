<?php

return [
    'dbal' => [
        'host' => 'postgres',
        'port' => '5432',
        'user' => 'dev',
        'password' => 'dev',
        'database' => 'dev'
    ]
];