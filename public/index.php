<?php

use Infrastructure\Application;

define('ROOT_DIR', dirname(__DIR__));
require_once ROOT_DIR . '/vendor/autoload.php';

$application = new Application();
$request = \Infrastructure\Http\Request::createFromGlobal();
$response = $application->handleRequest($request);
$response->send();
