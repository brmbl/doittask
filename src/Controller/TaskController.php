<?php

namespace App\Controller;


use App\Entity\Task;
use App\Entity\AuthToken as AuthTokenEntity;
use App\Entity\User;
use App\Model\ValueObject\AuthToken;
use App\Model\ValueObject\TaskStatus;
use App\Repository\AuthTokenRepository;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Infrastructure\DependencyInjection\Container;
use Infrastructure\DependencyInjection\Exception\ServiceNotFoundException;
use Infrastructure\Http\JsonResponse;
use Infrastructure\Http\Request;

class TaskController
{
    /**
     * @param Request   $request
     * @param Container $container
     *
     * @return JsonResponse
     * @throws ServiceNotFoundException
     */
    public function show(Request $request, Container $container)
    {
        if (!$request->query->has('token')) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'Required query param token is missing'
            ], 400);
        }

        $user = $this->getUserByToken(
            AuthToken::fromString($request->query->get('token')),
            $container
        );

        if (empty($user)) {
            return new JsonResponse([
                'error' => 'unauthorized',
                'message' => 'Wrong auth token'
            ], 401);
        }

        /** @var TaskRepository $taskRepository */
        $taskRepository = $container->get(TaskRepository::class);
        /** @var Task[] $tasks */
        $tasks = $taskRepository->findBy(['user_id' => $user->getId()]);
        $tasksResponse = [];
        foreach ($tasks as $task) {
            $tasksResponse[] = [
                'description' => $task->getDescription(),
                'status' => (string) TaskStatus::fromStatus($task->getStatus())
            ];
        }
        return new JsonResponse($tasksResponse);
    }

    /**
     * @param Request   $request
     * @param Container $container
     *
     * @return JsonResponse
     * @throws ServiceNotFoundException
     */
    public function add(Request $request, Container $container)
    {
        if (!$request->query->has('token')) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'Required query param token is missing'
            ], 400);
        }

        $user = $this->getUserByToken(
            AuthToken::fromString($request->query->get('token')),
            $container
        );

        if (empty($user)) {
            return new JsonResponse([
                'error' => 'unauthorized',
                'message' => 'Wrong auth token'
            ], 401);
        }

        $requestParams = $request->post->all();
        $errors = $this->validateAddData($requestParams);
        if (!empty($errors)) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'Required request params is missing - ' . implode(', ', $errors)
            ], 400);
        }

        $task = (new Task())->setUserId($user->getId())
            ->setDescription($requestParams['description'])
            ->setStatus(TaskStatus::STATUS_NEW);
        /** @var TaskRepository $taskRepository */
        $taskRepository = $container->get(TaskRepository::class);
        $taskId = $taskRepository->save($task);

        return new JsonResponse(['id' => $taskId]);
    }

    /**
     * @param AuthToken $token
     * @param Container $container
     *
     * @return User|null
     * @throws \Infrastructure\DependencyInjection\Exception\ServiceNotFoundException
     */
    private function getUserByToken(AuthToken $token, Container $container): ?User
    {
        /** @var AuthTokenRepository $tokenRepository */
        $tokenRepository = $container->get(AuthTokenRepository::class);
        /** @var AuthTokenEntity $authToken */
        $authToken = $tokenRepository->findOneBy(['token' => (string)$token]);
        if (empty($authToken)) {
            return null;
        }

        /** @var UserRepository $userRepository */
        $userRepository = $container->get(UserRepository::class);
        return $userRepository->findOneBy(['id' => $authToken->getUserId()]);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function validateAddData(array $data): array
    {
        return array_diff([
            'description'
        ], array_keys($data));
    }
}