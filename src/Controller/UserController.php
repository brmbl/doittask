<?php

namespace App\Controller;

use App\Entity\AuthToken as AuthTokenEntity;
use App\Entity\User;
use App\Model\ValueObject\AuthToken;
use App\Model\ValueObject\Email;
use App\Model\ValueObject\FirstName;
use App\Model\ValueObject\LastName;
use App\Model\ValueObject\Password;
use App\Repository\AuthTokenRepository;
use App\Repository\UserRepository;
use Infrastructure\DependencyInjection\Container;
use Infrastructure\DependencyInjection\Exception\ServiceNotFoundException;
use Infrastructure\Http\JsonResponse;
use Infrastructure\Http\Request;

class UserController
{
    /**
     * @param Request   $request
     * @param Container $container
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function register(Request $request, Container $container)
    {
        $requestParams = $request->post->all();
        $errors = $this->validateRegistrationData($requestParams);

        if (!empty($errors)) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'Requried request params is missing - ' . implode(', ', $errors)
            ], 400);
        }

        $email = Email::fromString($requestParams['email']);
        $password = Password::fromPlain($requestParams['password']);
        $firstName = FirstName::fromString($requestParams['first_name']);
        $lastName = LastName::fromString($requestParams['last_name']);
        $user = (new User)->setEmail((string)$email)
            ->setPassword((string)$password)
            ->setFirstName((string)$firstName)
            ->setLastName((string)$lastName);
        /** @var UserRepository $repository */
        $repository = $container->get(UserRepository::class);
        $exists = $repository->findOneBy(['email' => $user->getEmail()]);
        if ($exists) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'User with such email is already exists.'
            ], 400);
        }
        $id = $repository->save($user);
        $user->setId($id);
        $token = $this->createTokenForUser($user, $container);

        return new JsonResponse(['token' => (string)$token], 201);
    }

    /**
     * @param Request   $request
     * @param Container $container
     *
     * @return JsonResponse
     * @throws ServiceNotFoundException
     */
    public function login(Request $request, Container $container)
    {
        $requestParams = $request->post->all();
        $errors = $this->validateLoginData($requestParams);

        if (!empty($errors)) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'Requried request params is missing - ' . implode(', ', $errors)
            ], 400);
        }
        $email = Email::fromString($requestParams['email']);

        $userRepository = $container->get(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findOneBy([
            'email' => (string)$email
        ]);
        if (empty($user)) {
            return new JsonResponse([
                'error' => 'bad_request',
                'message' => 'User with such email is not exists.'
            ], 400);
        }

        $token = $this->createTokenForUser($user, $container);

        return new JsonResponse(['token' => (string)$token], 201);

    }

    private function createTokenForUser(User $user, Container $container): AuthToken
    {
        $tokenRepository = $container->get(AuthTokenRepository::class);
        $token = AuthToken::generateForUser($user);
        $tokenEntity = (new AuthTokenEntity())->setUserId($user->getId())->setToken((string)$token);
        $tokenRepository->save($tokenEntity);

        return $token;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function validateRegistrationData(array $data): array
    {
        return array_diff([
            'first_name',
            'last_name',
            'email',
            'password'
        ], array_keys($data));
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function validateLoginData(array $data): array
    {
        return array_diff([
            'email',
            'password'
        ], array_keys($data));
    }
}