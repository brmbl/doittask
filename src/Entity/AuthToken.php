<?php

namespace App\Entity;


use Infrastructure\ORM\Entity;

class AuthToken extends Entity
{
    /** @var int */
    private $userId;

    /** @var string */
    private $token;

    /**
     * @param int $userId
     *
     * @return AuthToken
     */
    public function setUserId(int $userId): AuthToken
    {
        $this->userId = $userId;
        return $this;
}

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param string $token
     *
     * @return AuthToken
     */
    public function setToken(string $token): AuthToken
    {
        $this->token = $token;
        return $this;
}

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}