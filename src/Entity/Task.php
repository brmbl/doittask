<?php

namespace App\Entity;


use Infrastructure\ORM\Entity;

class Task extends Entity
{
    /** @var int */
    private $userId;

    /** @var string */
    private $description;

    /** @var int */
    private $status;

    /**
     * @param int $userId
     *
     * @return Task
     */
    public function setUserId(int $userId): Task
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param mixed $description
     *
     * @return Task
     */
    public function setDescription($description): Task
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param int $status
     *
     * @return Task
     */
    public function setStatus(int $status): Task
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}