<?php

namespace App\Model\ValueObject;


class Password
{
    private $value;

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @param string $plain
     *
     * @return Password
     */
    public static function fromPlain(string $plain): Password
    {
        return new self(password_hash($plain, PASSWORD_DEFAULT));
    }

    /**
     * @param string $hashed
     *
     * @return Password
     */
    public static function fromHashed(string $hashed): Password
    {
        return new self($hashed);
    }

    /**
     * @param string $password
     * @param string $hash
     *
     * @return bool
     */
    public static function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}