<?php

namespace App\Model\ValueObject;


class Email
{
    /** @var string */
    private $value;

    /**
     * @param string $value
     *
     * @throws \Exception
     */
    private function __construct(string $value)
    {
        if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $value)){
            throw new \Exception("$value is not valid email address");
        }

        if (strlen($value) > 100) {
            throw new \Exception('Email value is too long, 100 characters is max');
        }

        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return Email
     * @throws \Exception
     */
    public static function fromString(string $value): Email
    {
        return new self ($value);
    }

    public function __toString(): string
    {
        return $this->value;
    }
}