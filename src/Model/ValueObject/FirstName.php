<?php

namespace App\Model\ValueObject;


class FirstName
{
    /** @var string */
    private $value;

    /**
     * @param string $value
     *
     * @throws \Exception
     */
    private function __construct(string $value)
    {
        if (strlen($value) > 50) {
            throw new \Exception('First name value is too long, 50 characters is max');
        }

        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return FirstName
     * @throws \Exception
     */
    public static function fromString(string $value): FirstName
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}