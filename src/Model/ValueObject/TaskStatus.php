<?php

namespace App\Model\ValueObject;


class TaskStatus
{
    public const STATUS_NEW = 0;

    public const STATUS_IN_PROGRESS = 1;

    public const STATUS_DONE = 2;

    public const STATUS_TEXT_NEW = 'new';

    public const STATUS_TEXT_IN_PROGRESS = 'in_progress';

    public const STATUS_TEXT_DONE = 'done';

    private static $textToStatusMap = [
        self::STATUS_TEXT_NEW => self::STATUS_NEW,
        self::STATUS_TEXT_IN_PROGRESS => self::STATUS_IN_PROGRESS,
        self::STATUS_TEXT_DONE => self::STATUS_DONE
    ];

    private static $statusToTextMap = [
        self::STATUS_NEW => self::STATUS_TEXT_NEW,
        self::STATUS_IN_PROGRESS => self::STATUS_TEXT_IN_PROGRESS,
        self::STATUS_DONE => self::STATUS_TEXT_DONE
    ];

    /** @var int */
    private $status;

    /**
     * @param int $status
     */
    private function __construct(int $status)
    {
        $this->status = $status;
    }

    /**
     * @param string $text
     *
     * @return TaskStatus
     * @throws \Exception
     */
    public static function fromText(string $text): TaskStatus
    {
        if (!in_array($text, [
            self::STATUS_TEXT_NEW,
            self::STATUS_TEXT_IN_PROGRESS,
            self::STATUS_TEXT_DONE,
        ])) {
            throw new \Exception("Undefined task status $text");
        }

        return new self (self::$textToStatusMap[$text]);
    }

    /**
     * @param int $status
     *
     * @return TaskStatus
     * @throws \Exception
     */
    public static function fromStatus(int $status): TaskStatus
    {
        if (!in_array($status, [
            self::STATUS_NEW,
            self::STATUS_IN_PROGRESS,
            self::STATUS_DONE
        ])) {
            throw new \Exception("Undefined task status $status");
        }

        return new self($status);
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return self::$statusToTextMap[$this->status];
    }
}