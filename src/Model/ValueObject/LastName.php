<?php

namespace App\Model\ValueObject;


class LastName
{
    /** @var string */
    private $value;

    /**
     * @param string $value
     *
     * @throws \Exception
     */
    private function __construct(string $value)
    {
        if (strlen($value) > 50) {
            throw new \Exception('Last name value is too long, 50 characters is max');
        }

        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return LastName
     * @throws \Exception
     */
    public static function fromString(string $value): LastName
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}