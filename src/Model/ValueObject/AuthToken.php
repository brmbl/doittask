<?php

namespace App\Model\ValueObject;

use App\Entity\User;

class AuthToken
{
    /** @var string */
    private $token;

    /**
     * @param string $token
     */
    private function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @param User $user
     *
     * @return AuthToken
     */
    public static function generateForUser(User $user): AuthToken
    {
        $token = md5(uniqid($user->getEmail(), true));

        return new self($token);
    }

    /**
     * @param string $token
     *
     * @return AuthToken
     */
    public static function fromString(string $token): AuthToken
    {
        return new self($token);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->token;
    }
}