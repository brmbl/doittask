<?php

namespace App\Repository;


use App\Entity\User;
use Infrastructure\DBAL\EntityRepository;
use PDO;

class UserRepository extends EntityRepository
{

    /**
     * @param User $user
     *
     * @return int
     * @throws \Exception
     */
    public function save(User $user): int
    {
        $sth = $this->connection->pdo()->prepare("
              INSERT INTO {$this->tableName} 
                (email, password, first_name, last_name)
                VALUES 
                (:email, :password, :first_name, :last_name)
         ");
        $sth->bindParam(':email', $user->getEmail());
        $sth->bindParam('password', $user->getPassword());
        $sth->bindParam('first_name', $user->getFirstName());
        $sth->bindParam('last_name', $user->getLastName());
        try {
            $sth->execute();
            return $this->connection->pdo()->lastInsertId();
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param array $criteria
     *
     * @return User|null
     */
    public function findOneBy(array $criteria): ?User
    {
        $criteriaKeys = array_keys($criteria);
        $criteriaValues = array_values($criteria);
        $firstKey = array_shift($criteriaKeys);
        $firstValue = array_shift($criteriaValues);
        $query = "SELECT * FROM {$this->tableName} WHERE {$firstKey} = ?";

        foreach ($criteriaKeys as $key) {
            $query .= " AND {$key} = ?";
        }

        $query .= ' LIMIT 1';
        $sth = $this->connection->pdo()->prepare($query);
        $sth->bindParam(1, $firstValue, PDO::PARAM_STR);
        for ($i=2; $i < count($criteriaValues)+2; $i++) {
            $index = $i - 2;
            $sth->bindParam($i, $criteriaValues[$index], PDO::PARAM_STR);
        }
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($rows)) {
            return $this->hydrator->hydrate($rows[0], $this->entity);
        }

        return null;
    }
}