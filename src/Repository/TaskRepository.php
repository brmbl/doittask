<?php

namespace App\Repository;


use App\Entity\Task;
use Infrastructure\DBAL\EntityRepository;
use PDO;

class TaskRepository extends EntityRepository
{
    /**
     * @param Task $task
     *
     * @return int
     * @throws \Exception
     */
    public function save(Task $task): int
    {
        $sth = $this->connection->pdo()->prepare("
              INSERT INTO {$this->tableName} 
                (user_id, description, status)
                VALUES 
                (:user_id, :description, :status)
         ");

        $sth->bindParam('user_id', $task->getUserId());
        $sth->bindParam('description', $task->getDescription());
        $sth->bindParam('status', $task->getStatus());

        try {
            $sth->execute();
            return $this->connection->pdo()->lastInsertId();
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria): array
    {
        $criteriaKeys = array_keys($criteria);
        $criteriaValues = array_values($criteria);
        $firstKey = array_shift($criteriaKeys);
        $firstValue = array_shift($criteriaValues);
        $query = "SELECT * FROM {$this->tableName} WHERE {$firstKey} = ?";

        foreach ($criteriaKeys as $key) {
            $query .= " AND {$key} = ?";
        }

        $sth = $this->connection->pdo()->prepare($query);
        $sth->bindParam(1, $firstValue, PDO::PARAM_STR);
        for ($i=2; $i < count($criteriaValues)+2; $i++) {
            $index = $i - 2;
            $sth->bindParam($i, $criteriaValues[$index], PDO::PARAM_STR);
        }
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($rows)) {
            return $this->hydrator->hydrateAll($rows, $this->entity);
        }

        return [];
    }
}