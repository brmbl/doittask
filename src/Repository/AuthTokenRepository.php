<?php

namespace App\Repository;


use App\Entity\AuthToken;
use Infrastructure\DBAL\EntityRepository;
use PDO;

class AuthTokenRepository extends EntityRepository
{
    /**
     * @param AuthToken $token
     *
     * @throws \Exception
     */
    public function save(AuthToken $token): void
    {
        $sth = $this->connection->pdo()->prepare("
              INSERT INTO {$this->tableName} 
                (user_id, token)
                VALUES 
                (:user_id, :token)
         ");
        $sth->bindParam('user_id', $token->getUserId());
        $sth->bindParam('token', $token->getToken());
        try {
            $sth->execute();
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function findOneBy($criteria)
    {
        $criteriaKeys = array_keys($criteria);
        $criteriaValues = array_values($criteria);
        $firstKey = array_shift($criteriaKeys);
        $firstValue = array_shift($criteriaValues);
        $query = "SELECT * FROM {$this->tableName} WHERE {$firstKey} = ?";

        foreach ($criteriaKeys as $key) {
            $query .= " AND {$key} = ?";
        }

        $query .= ' LIMIT 1';
        $sth = $this->connection->pdo()->prepare($query);
        $sth->bindParam(1, $firstValue, PDO::PARAM_STR);
        for ($i=2; $i < count($criteriaValues)+2; $i++) {
            $index = $i - 2;
            $sth->bindParam($i, $criteriaValues[$index], PDO::PARAM_STR);
        }
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($rows)) {
            return $this->hydrator->hydrate($rows[0], $this->entity);
        }

        return null;
    }
}