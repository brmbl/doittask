<?php

namespace Infrastructure\Helper;


class StringHelper
{
    /**
     * @param string $value
     *
     * @return string
     */
    public static function camel($value): string
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));
        return lcfirst(str_replace(' ', '', $value));
    }
}