<?php

namespace Infrastructure\Http;


class JsonResponse extends Response
{
    public function __construct($content = '', int $statusCod = 200, $headers = [])
    {
        $headers['Content-Type'] = 'application/json';
        parent::__construct($content, $statusCod, $headers);
    }
}