<?php

namespace Infrastructure\Http\ValueObject;


class Query
{
    /**
     * @var array
     */
    private $params;

    /**
     * @param array $params
     */
    private function __construct(array $params)
    {
        foreach ($params as $key => $param) {
            $this->params[$key] = filter_input(
                INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS
            );
        }
    }

    /**
     * @param array $params
     *
     * @return Query
     */
    public static function fromArray(array $params): Query
    {
        return new self($params);
    }

    /**
     * @param string $key
     *
     * @return null|string
     */
    public function get(string $key): ?string
    {
        if ($this->has($key)){
            return $this->params[$key];
        }

        return null;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return in_array($key, array_keys($this->params));
    }
}