<?php

namespace Infrastructure\Http\ValueObject;


class Post
{
    /** @var array */
    private $params = [];

    /**
     * @param array $params
     */
    private function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @param array $params
     *
     * @return Post
     */
    public static function fromArray(array $params): Post
    {
        return new self($params);
    }

    /**
     * @param string $key
     *
     * @return null|string
     */
    public function get(string $key): ?string
    {
        if ($this->has($key)){
            return $this->params[$key];
        }

        return null;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return in_array($key, array_keys($this->params));
    }

    public function all()
    {
        return $this->params;
    }
}