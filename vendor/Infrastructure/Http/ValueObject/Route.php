<?php

namespace Infrastructure\Http\ValueObject;


class Route
{
    /** @var string */
    private $controller;

    /** @var string */
    private $action;

    /**
     * @param string $routeString
     *
     * @throws \Exception
     */
    private function __construct(string $routeString)
    {
        if (substr_count($routeString, '@') !== 1) {
            throw new \Exception('Route string format invalid');
        }

        $routeParts = explode('@', $routeString);
        $this->controller = $routeParts[0];
        $this->action = $routeParts[1];
    }

    /**
     * @param string $routeString
     *
     * @return Route
     * @throws \Exception
     */
    public static function fromString(string $routeString): Route
    {
        return new self($routeString);
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}