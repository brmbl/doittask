<?php

namespace Infrastructure\Http;

use Infrastructure\Http\ValueObject\Route;

class Router
{
    /** @var array */
    private $routes;

    /**
     * @return Route|null
     * @throws \Exception
     */
    public function getRoute(): ?Route
    {
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $uri = trim($_SERVER['REQUEST_URI'], '/');
        $uriParts = explode('?', $uri);
        $routes = $this->getRoutes();
        if (!in_array($requestMethod, array_keys($routes))){
            throw new \Exception('Request method is undefined');
        }
        $routesMatchedMethod = $routes[$requestMethod];
        foreach ($routesMatchedMethod as $key => $route) {
            if ($key === $uriParts[0]) {
                return Route::fromString($route);
            }
        }

        return null;
    }

    /**
     * @return array
     */
    private function getRoutes(): array
    {
        if (empty($this->routes)) {
            $this->routes = yaml_parse_file(ROOT_DIR . '/config/routing.yaml');
        }

        return $this->routes;
    }
}