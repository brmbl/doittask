<?php

namespace Infrastructure\Http;

use Infrastructure\Http\ValueObject\Post;
use Infrastructure\Http\ValueObject\Query;

class Request
{
    /** @var Query */
    public $query;

    /** @var Post */
    public $post;

    /**
     * @param array $query
     * @param array $post
     */
    private function __construct(array $query, array $post)
    {
        $this->query = Query::fromArray($query);
        $raw = json_decode(file_get_contents('php://input'), true);
        if (!empty($raw)) {
            $post = array_merge($raw, $post);
        }
        $this->post = Post::fromArray($post);
    }

    /**
     * @return Request
     */
    public static function createFromGlobal(): Request
    {
        return new self ($_GET, $_POST);
    }
}