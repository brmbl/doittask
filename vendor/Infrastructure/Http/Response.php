<?php

namespace Infrastructure\Http;

class Response
{
    private $headers;
    
    private $content;
    
    private $statusCode;
    
    private $statusText;

    private $statusTexts = [
        200 => 'OK',
        201 => 'Created',
        400 => 'Bad Request',
        404 => 'Not Found',
        500 => 'Internal Server Error'
    ];

    public function __construct($content = '', int $statusCode = 200, array $headers = [])
    {
        $this->content = is_array($content) ? json_encode($content) : $content;
        $this->statusCode = $statusCode;
        if (in_array($statusCode, array_keys($this->statusTexts))) {
            $this->statusText = $this->statusTexts[$statusCode];
        } else {
            $this->statusText = 'Unknown';
        }

        $this->headers = $headers;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();

        return $this;
    }

    public function sendHeaders()
    {
        if (headers_sent()) {
            return $this;
        }

        foreach ($this->headers as $key => $value) {
            header($key . ':' . $value);
        }

        header(sprintf('HTTP/1.1 %s %s', $this->statusCode, $this->statusText));
    }

    public function sendContent()
    {
        echo $this->content;

        return $this;
    }
}