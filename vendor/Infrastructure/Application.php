<?php

namespace Infrastructure;

use Infrastructure\DependencyInjection\Container;
use Infrastructure\Http\JsonResponse;
use Infrastructure\Http\Request;
use Infrastructure\Http\Response;
use Infrastructure\Http\Router;
use Infrastructure\Http\ValueObject\Route;

class Application
{
    /** @var Router */
    private $router;

    /** @var Container */
    private $container;

    public function __construct()
    {
        $services   = include dirname(__DIR__).'/../config/services.php';
        $parameters   = include dirname(__DIR__).'/../config/parameters.php';
        $this->container = new Container($services, $parameters);
        $this->router = new Router();
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function handleRequest(Request $request): Response
    {
        try {
            $route = $this->router->getRoute();
        } catch (\Exception $e) {
            return new JsonResponse([
                'error' => 'router.cannot_find_routes',
                'message' => $e->getMessage(),
            ], 500);
        }

        if (!$route instanceof Route) {
            return new JsonResponse(['error' => 'page not found'], 404);
        }

        $controllerName = $route->getController();
        $response = call_user_func_array([new $controllerName, $route->getAction()], [$request, $this->container]);

        if (!$response instanceof Response) {
            return new JsonResponse();
        }

        return $response;
    }
}