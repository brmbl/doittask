<?php

namespace Infrastructure\ORM\Hydrator;


interface HydratorInterface extends ExtractionInterface, HydrationInterface
{

}