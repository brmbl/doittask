<?php

namespace Infrastructure\ORM\Hydrator;


interface ExtractionInterface
{
    /**
     * @param object $object
     * @return array
     */
    public function extract($object): array ;
}