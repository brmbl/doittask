<?php

namespace Infrastructure\ORM\Hydrator;


use Infrastructure\Helper\StringHelper;

class ObjectHydrator implements HydratorInterface
{
    /**
     * @param array  $array
     * @param object $object
     *
     * @return object
     */
    public function hydrate(array $array, $object)
    {
        $methods = array_flip(get_class_methods(get_class($object)));

        foreach ($array as $key => $value) {
            $method = StringHelper::camel('set_' . $key);
            if (isset($methods[$method])) {
                $object->$method($value);
            }
        }

        return $object;
    }

    /**
     * @param array  $array
     * @param object $object
     *
     * @return array
     */
    public function hydrateAll(array $array, $object): array
    {
        $result = [];

        foreach ($array as $item) {
            $result[] = $this->hydrate($item, $object);
        }

        return $result;
    }

    /**
     * @param object $object
     *
     * @return array
     */
    public function extract($object): array
    {
        $array = [];
        $methods = get_class_methods(get_class($object));

        foreach ($methods as $method) {
            preg_match(' /^(get)(.*?)$/i', $method, $matches);
            if (!isset($matches[2])) {
                continue;
            }
            $key = StringHelper::camel($matches[2]);
            $array[$key] = $object->$method();
        }

        return $array;
    }
}