<?php

namespace Infrastructure\ORM\Hydrator;


interface HydrationInterface
{
    /**
     * @param array $data
     * @param object $object
     * @return object
     */
    public function hydrate(array $data, $object);
}