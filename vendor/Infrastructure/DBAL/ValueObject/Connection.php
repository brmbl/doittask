<?php

namespace Infrastructure\DBAL\ValueObject;


use PDO;

class Connection
{
    /** @var PDO */
    private $pdo;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $conn = "pgsql:host={$config->getHost()};port={$config->getPort()};dbname={$config->getDatabase()}"
            . ";user={$config->getUser()};password={$config->getPassword()}";

        $this->pdo = new PDO($conn);
    }

    /**
     * @return PDO
     */
    public function pdo(): PDO
    {
        return $this->pdo;
    }
}