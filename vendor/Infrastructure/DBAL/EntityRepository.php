<?php

namespace Infrastructure\DBAL;

use Infrastructure\DBAL\ValueObject\Connection;
use Infrastructure\ORM\Entity;
use Infrastructure\ORM\Hydrator\ObjectHydrator;

class EntityRepository
{
    /** @var Connection */
    protected $connection;

    /** @var Entity */
    protected $entity;

    /** @var string */
    protected $tableName;

    /** @var ObjectHydrator */
    protected $hydrator;

    /**
     * @param Connection $connection
     * @param Entity     $entity
     * @param string     $tableName
     */
    public function __construct(Connection $connection, Entity $entity, string $tableName)
    {
        $this->connection = $connection;
        $this->entity = $entity;
        $this->tableName = $tableName;
        $this->hydrator = new ObjectHydrator();
    }
}