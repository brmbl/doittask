<?php

function __autoload(string $class)
{
    $classParts = explode('\\', $class);
    $className = array_pop($classParts);
    $appRoot = dirname(__DIR__) . '/src';
    $vendorRoot = __DIR__;

    if ($classParts[0] === 'App') {
        array_shift($classParts);
        $search = $appRoot;
        foreach ($classParts as $dir) {
            $search .= "/$dir";
        }
    } else {
        $search = $vendorRoot;
        foreach ($classParts as $dir) {
            $search .= '/' . ucfirst($dir);
        }
    }

    $search .= '/' . $className . '.php';

    if (is_file($search)) {
        return include_once $search;
    }

    throw new \Exception('Cannot autoload class ' . $class);
}