#Simple REST application
##Instalation
Clone repository to your machine.
Run `docker-compose up -d` in order to bring up the docker containers,
by default it use nginx port `80` and postgres port `5432` 
so make sure that this ports are available or change configuration.

Once you run docker, you can use application, list of available end-points located below:

##Base url
`http://localhost`
##Registration
- #####URL
    /registration
- #####Method
    `POST`
- #####Data params
    ```json
    {
    	"first_name" : "John",
    	"last_name" : "Snow",
    	"email" : "john.snow@winterfel.com",
    	"password" : "secret"
    }
    ```        
- #####Success response
    Code `201` <br>
    Content  `{"token":"7a6b8d1a7d033fa7af14f4a7932e12ee"}`   
     
##Login
- #####URL
    /login
- #####Method
    `POST`
- #####Data params
    ```json
    {
    	"email" : "john.snow@winterfel.com",
    	"password" : "secret"
    }
    ```        
- #####Success response
    Code `201` <br>
    Content  `{"token":"7a6b8d1a7d033fa7af14f4a7932e12ee"}`
     
##Add task
- #####URL
    /tasks?token=7a6b8d1a7d033fa7af14f4a7932e12ee
- #####Method
    `POST`
- #####Data params
    ```json
    {
    	"description" : "some action to do"
    }
    ```        
- #####Success response
    Code `201` <br>
    Content  `{"id": 1}`
     
##Show tasks
- #####URL
    /tasks?token=7a6b8d1a7d033fa7af14f4a7932e12ee
- #####Method
    `GET`
        
- #####Success response
    Code `200` <br>
    Content  
     ```json
        [
          {
        	"description" : "some action to do",
      	    "status": "new"
          },
          {
        	"description" : "another action to do",
      	    "status": "new"
          }
        ]
     ```  
    
#From author
Unfortunately there is no endpoint for delete or mark as done, because I haven't time for it.
But I guess it no big deal to add two more endpoints like `DELETE` and `PATCH`.
